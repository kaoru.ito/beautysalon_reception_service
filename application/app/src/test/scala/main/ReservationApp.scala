package main

import java.time.{LocalDate, LocalDateTime}

import com.google.inject.Inject
import org.scalatest.{DiagrammedAssertions, FlatSpec}
import service.ReservationServiceImple

class ReservationApp @Inject()(reservationService: ReservationServiceImple) extends FlatSpec with DiagrammedAssertions {

  it should "formatできたかできなかったかをOption(指定した型)で返す" in {

    assert(reservationService.formatDate("2018-05-30 15:00")(reservationService.todatetime).get
      === LocalDateTime.of(2018, 5, 30, 15, 0))

    assert(reservationService.formatDate("1996-11-27")(reservationService.todate).get
      === LocalDate.of(1996, 11, 27))
  }
}
