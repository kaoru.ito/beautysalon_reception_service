package main.module

import com.google.inject.AbstractModule
import service._

class ReservationModule extends AbstractModule {

  override def configure(): Unit = {
    //ここでバインド A => B
    bind(classOf[ReservationService]).to(classOf[ReservationServiceImple])
  }
}

