package main

import java.time.format.DateTimeFormatter
import java.time.{LocalDate, LocalDateTime}

import com.google.inject.{Inject, Singleton}
import main.Main.reservationApp

import scala.io.StdIn
import model.{Birthday, Customer, Menu, Name, PhoneNumber, Price}
import service._

@Singleton
class ReservationApp @Inject()(reservation: ReservationService) {

  def run() = {
    val app         = reservationApp.askAppDay()
    val menu        = reservationApp.askMenu()
    val stylist     = reservationApp.askStylist()
    val stylistName = if(stylist == Menu.STYLIST_MONEY) reservationApp.askStylistName() else "なし"
    val name        = reservationApp.customerName()
    val phone       = reservationApp.customerPhone()
    val birthday    = reservationApp.customerBirth()
    val customer    = Customer(name,phone,Birthday(birthday.value,birthday.age))

    println("----------------------")
    println("以下通りご予約承りました")
    println(s"お客様氏名: ${customer.name.value}")
    println(s"電話番号: ${customer.phoneNumber.value}")
    println(s"生年月日: ${customer.birthDate.value}\n")
    println(s"ご予約時: ${app}")
    println(s"メニュー: ${menu.name}")
    println(s"指名スタイリスト: ${stylistName}\n")
    println(s"料金: ${reservationApp.calclate(stylist,menu.price,birthday.age)}")
    println("----------------------")
  }

  private val todatetime = (d: String) => LocalDateTime.parse(d, DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm"))
  private val todate     = (d: String) =>     LocalDate.parse(d, DateTimeFormatter.ofPattern("uuuu-MM-dd"))

  private def calclate(stylistPrice: Int, menuPrice: Int, age: Int): Int = {
    val isStylist = Price(stylistPrice)
    val basicPrice = isStylist.calclatePrice(Price(menuPrice))
    val isDiscount = if (age < 19) Price((basicPrice.money * 0.8).toInt) else basicPrice
    isDiscount.money
  }

  // 予約日を聞く
  private def askAppDay():LocalDateTime = {
    println("予約日時はいつにしますか? （例： 2018-05-30 15:00)")
    val date = StdIn.readLine()
    reservation.formatDate(date)(todatetime) match {
      case Some(x) => x
      case None    => {
        println("指定したフォーマットで入力してください")
        askAppDay()
      }
    }
  }

  // メニューを聞く
  private def askMenu(): Menu = {
    println("ご希望のカット番号を（半角）でお選びください")
    for (menu <- Menu.all) println(s"${menu.id}. ${menu.name} (${menu.price}円)")
    val id = StdIn.readLine
    reservation.getMenu(reservation.numFormat(id)) match {
      case Some(x) => x
      case None    => {
        println("メニューの番号を入力してください")
        askMenu()
      }
    }
  }

  // スタイリストの料金
  private def askStylist(): Int = {
    println("スタイリストの指名はございますか？(指名料：５００円) [YES or NO]")
    val res = StdIn.readLine()
    reservation.withStylist(res) match {
      case Right(true)  => Menu.STYLIST_MONEY
      case Right(false) => Menu.STYLIST_MONEY_ZERO
      case Left(_) => {
        println("YES or NOで答えてください")
        askStylist()
      }
    }
  }

  // スタイリスト名
  private def askStylistName(): String = {
    println("指名するスタイリストの名前を入力してください")
    StdIn.readLine()
  }

  // お客の名前
  private def customerName(): Name = {
    println("お客様のお名前を入力してください")
    val name = StdIn.readLine()
    reservation.validateName(name) match {
      case true   => Name(name)
      case false  => {
        println("0文字以上入力してください")
        customerName()
      }
    }
  }

  // 客の電話番号
  private def customerPhone(): PhoneNumber = {
    println("電話番号を入力してください")
    val phone = StdIn.readLine()
    reservation.validatePhone(phone) match {
      case Some(date) => PhoneNumber(phone)
      case None       => {
        println("11桁の電話番号を入力してください")
        customerPhone()
      }
    }
  }

  // 客の生年月日
  private def customerBirth(): Birthday = {
    println("生年月日を入力してください (例: 1996-11-27)")
    val birth = StdIn.readLine()
    reservation.formatDate(birth)(todate) match {
      case Some(date) => Birthday(date,reservation.underEighteen(date))
      case None       => {
        println("指定したフォーマットで入力してください")
        customerBirth()
      }
    }
  }
}
