package main

import com.google.inject.{Guice, Injector}
import main.module.ReservationModule

object Main {

  val injector: Injector = Guice.createInjector(new ReservationModule)
  val reservationApp = injector.getInstance(classOf[ReservationApp])

  def main(args: Array[String]): Unit = {
    reservationApp.run()
  }
}
