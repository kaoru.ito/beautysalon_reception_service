package service

import java.time.LocalDate

import model.Menu

trait ReservationService {
  def getMenu      (key:        Int):                    Option[Menu]
  def withStylist  (boolString: String):                 Either[Exception,Boolean]
  def underEighteen(birth:      LocalDate):              Int
  def validateName (name:       String):                 Boolean
  def validatePhone(num:        String):                 Option[String]
  def formatDate[A](date:       String)(f: String => A): Option[A]
  def numFormat    (str:        String):                 Int
}
