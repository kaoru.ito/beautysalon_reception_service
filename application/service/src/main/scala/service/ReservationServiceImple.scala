package service

import java.time.LocalDate
import java.time.temporal.ChronoUnit

import scala.util.{Failure, Success, Try}
import model.Menu

class ReservationServiceImple extends ReservationService {

  // formatできたかできなかったかをOption(指定した型)で返す
  def formatDate[A](date: String)(f: String => A): Option[A] = {
    Try(f(
      date)) match {
      case Success(value)     => Some(value)
      case Failure(exception) => None
    }
  }

  // menu選ぶ際のチェックバリデーション 0 -> None
  def numFormat(str: String): Int = {
    Try(str.toInt) match {
      case Success(value)     => value
      case Failure(exception) => 0
    }
  }

  // メニューを取得
  def getMenu(key: Int): Option[Menu] = Menu.all.find(n => n.id == key)

  // 指名の有無
  def withStylist(boolString: String): Either[Exception,Boolean] = {
    boolString match {
      case "YES" => Right(true)
      case "NO"  => Right(false)
      case _     => Left(new Exception)
    }
  }

  // 18歳未満は20％オフ
  def underEighteen(birth: LocalDate): Int = {
    ChronoUnit.YEARS.between(birth, LocalDate.now).toInt
  }

  // 名前が空文字の場合再入力
  def validateName(name: String): Boolean = {
    name match {
      case "" => false
      case _  => true
    }
  }

  // 11桁ではない時に再入力
  def validatePhone(num: String): Option[String] = {
    num.matches("""\d{11}""") match {
      case true  => Some(num)
      case false => None
    }
  }
}
