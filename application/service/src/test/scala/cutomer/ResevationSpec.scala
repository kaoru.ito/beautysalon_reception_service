package reservation

import java.time.{LocalDate, LocalDateTime}

<<<<<<< HEAD:application/service/src/test/scala/cutomer/ResevationSpec.scala
import model.Menu
import org.scalatest.{DiagrammedAssertions, EitherValues, FlatSpec, Matchers}
import service.ReservationServiceImple
=======
import customer.model.Menu
import cutomer.service.Reservation
import org.scalatest.{DiagrammedAssertions, EitherValues, FlatSpec, Matchers}
>>>>>>> develop:src/test/scala/cutomer/ResevationSpec.scala

class ReservationSpec extends FlatSpec with Matchers with DiagrammedAssertions with EitherValues {

  "Reservation" should "美容院予約アプリケーション" in {
  }

<<<<<<< HEAD:application/service/src/test/scala/cutomer/ResevationSpec.scala
  val resevation = new ReservationServiceImple
=======
  val resevation = new Reservation

  it should "formatできたかできなかったかをOption(指定した型)で返す" in {
    assert(resevation.formatDate("2018-05-30 15:00")(resevation.todatetime) === Some(LocalDateTime.of(2018,5,30,15,0)))
    assert(resevation.formatDate("1996-11-27")(resevation.todate)           === Some(LocalDate.of(1996,11,27)))
    assert(resevation.formatDate("ahahaha")(resevation.todate)              === None)
  }
>>>>>>> develop:src/test/scala/cutomer/ResevationSpec.scala

  it should "Menuからid指定でget" in {
    assert(resevation.getMenu(1).get === Menu.Cut)
    resevation.getMenu(1).get shouldBe Menu.Cut
    resevation.getMenu(1).get should be (Menu.Cut)
  }

  it should "スタイリストの有無をExceptionとBooleanを返す" in {
    assert(resevation.withStylist("YES") === Right(true))
    assert(resevation.withStylist("NO")  === Right(false))
    resevation.withStylist("aaa").left.value shouldBe an[Exception]
  }

  it should "年齢を返すメソッド" in {
    assert(resevation.underEighteen(LocalDate.of(1996,11,27)) === 22) // 19才以上
    assert(resevation.underEighteen(LocalDate.of(2000,11,27)) === 18)  // 18以下
  }

  it should "名前が空文字かどうかをBooleanで返す" in {
    assert(resevation.validateName("")      === false)
    assert(resevation.validateName("kaoru") === true)
  }

  it should "電話番号があるかないかをOptionで返す（11桁ではない時に再入力）" in {
    assert(resevation.validatePhone("08000000000") === Some("08000000000"))
    assert(resevation.validatePhone("080") === None)
  }
}
