package model

import java.time.LocalDate

case class Customer(
  name:         Name,
  phoneNumber:  PhoneNumber,
  birthDate:    Birthday,
)

case class Name        (value:   String)
case class PhoneNumber (value:   String)
case class Birthday    (value:   LocalDate, age: Int)

