package model

case class Price(money:Int) {
  def calclatePrice(that:Price):Price = {
    Price(money + that.money)
  }
}

sealed abstract class Menu(val id:Int,val name: String ,val price: Int)
object Menu {
  case object Cut      extends Menu(1,"カット",       5000)
  case object CutColor extends Menu(2,"カットカラー", 10000)
  case object CutParm  extends Menu(3,"カットパーマ", 10000)
  case object CutOnly  extends Menu(4,"カットのみ",   7000)
  case object ParmOnly extends Menu(5,"パーマのみ",   7000)

  //全てのメニュー
  val all = Seq(Cut,CutColor,CutParm,CutOnly,ParmOnly)

  // スタイリストの料金
  val STYLIST_MONEY      = 500
  val STYLIST_MONEY_ZERO = 0
}

