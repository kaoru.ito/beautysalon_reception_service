import Dependencies._

ThisBuild / scalaVersion     := "2.12.8"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.example"
ThisBuild / organizationName := "example"

lazy val root = (project in file("."))
  .aggregate(app,model,service)
  .settings(
    name := "beautysalon_reception_service",
  )

lazy val commonSettings = Seq (
  libraryDependencies ++= Seq(
    "com.google.inject" % "guice" % "4.2.2",
    "org.scalatest" %% "scalatest" % "3.0.5" % "test"
  )
)

lazy val app = (project in file("application/app"))
  .settings(
    name := "app",
    commonSettings
  )
  .dependsOn(service,model)

lazy val service = (project in file("application/service"))
  .settings(
    name := "service",
    commonSettings
  )
  .dependsOn(model)

lazy val model = (project in file("application/model"))
  .settings(
    name := "model",
    commonSettings
  )

